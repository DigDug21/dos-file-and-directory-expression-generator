# DOS File and Directory Expression Generator

This program generates a list of user-selected files, directories, child files, and child directories copy-able to clipboard.


This can be rather helpful for users of 2BrightSpark's SyncBack software for including/excluding certain types of files/folders.